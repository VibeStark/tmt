var gulp = require('gulp');

var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');


var data = require('gulp-data');
var pug = require('gulp-pug');
var path = require('path');
var fs = require('fs');


gulp.task('sassBuild', function(){
    gulp.src('./src/sass/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('fontsBuild', function(){
    return gulp.src('./src/fonts/**/*.*')
        .pipe(gulp.dest('./dist/fonts'))
});

gulp.task('pugBuild', function buildHTML(){
    return gulp.src('./src/pug/*.pug')
        .pipe(data( function(file) {
            return JSON.parse(fs.readFileSync('./src/pug/data.json'));
        } ))
        .pipe(
        pug({
            pretty: false
        })
    )
        .pipe(gulp.dest('./dist'));
});

var scriptsList = [
    './bower_components/jquery/dist/jquery.js',
    './bower_components/device.js/lib/device.js',
    './bower_components/responsive-bootstrap-toolkit/dist/bootstrap-toolkit.js',
    './bower_components/owl.carousel/dist/owl.carousel.js',
    './src/scripts/particles.js',
    './src/scripts/revolution/jquery.themepunch.revolution.js',
    './src/scripts/revolution/jquery.themepunch.tools.min.js',
    './src/scripts/revolution/assets/revolution.extension.actions.js',
    './src/scripts/revolution/assets/revolution.extension.carousel.js',
    './src/scripts/revolution/assets/revolution.extension.kenburn.js',
    './src/scripts/revolution/assets/revolution.extension.layeranimation.js',
    './src/scripts/revolution/assets/revolution.extension.migration.js',
    './src/scripts/revolution/assets/revolution.extension.navigation.js',
    './src/scripts/revolution/assets/revolution.extension.parallax.js',
    './src/scripts/revolution/assets/revolution.extension.slideanims.js',
    './src/scripts/revolution/assets/revolution.extension.video.js',
    './src/scripts/materialize/global.js',
    './src/scripts/materialize/forms.js',
    './src/scripts/rev-init.js',
    './src/scripts/part-init.js',
    './src/scripts/script.js'
];


gulp.task('scriptsBuild', function(){
    gulp.src(scriptsList)
        .pipe(concat('build.js'))
        .pipe(gulp.dest('./src/scripts'))
});

gulp.task('uglifyScript', function(){
    gulp.src('./src/scripts/build.js')
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
});

gulp.task('imageBuild', function(){
    gulp.src('./src/images/**{.jpg, .png}')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/images'))
});

gulp.task('build', ['sassBuild', 'fontsBuild', 'pugBuild', 'scriptsBuild', 'uglifyScript', 'imageBuild'], function(){

});