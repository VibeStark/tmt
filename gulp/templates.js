var gulp = require('gulp');

var data = require('gulp-data');
var pug = require('gulp-pug');
var path = require('path');
var fs = require('fs');

gulp.task('pug', function buildHTML(){
    return gulp.src('./src/pug/*.pug')
        .pipe(data( function(file) {
            return JSON.parse(fs.readFileSync('./src/pug/data.json'));
        } ))
        .pipe(
        pug({
            pretty: true,
            doctype: '<!doctype html>'
        })
    )
        .pipe(gulp.dest('./temp/pug/'));
});